from django.db import models

class Update(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    activity = models.TextField()

class Comment(models.Model):
    comment = models.TextField()
    update = models.ForeignKey(Update, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
