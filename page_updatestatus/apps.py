from django.apps import AppConfig


class PageUpdatestatusConfig(AppConfig):
    name = 'page_updatestatus'
