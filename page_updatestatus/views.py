from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Update, Comment
from .forms import Update_Form, Comment_Form


response = {}
user_name = 'Kelompok 9 PPW-C'
def index(request):
    response['user_name'] = user_name
    updates = Update.objects.all().order_by('-date')
    comments = Comment.objects.all().order_by('-date')
    response['updates'] = updates
    response['comments'] = comments
    response['update_form'] = Update_Form
    response['comment_form'] = Comment_Form
    return render(request, 'page_updatestatus.html', response)

def update_activity(request):
    form = Update_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['activity'] = request.POST['activity']
        updates = Update(activity=response['activity'])
        updates.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def update_comment(request,pk):
    update = Update.objects.get(pk=pk)
    form = Comment_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['comment'] = request.POST['comment']
        comments = Comment(comment=response['comment'])
        comments.update = update
        comments.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def clear(request,object_id):
    clears = Update.objects.get(pk=object_id)
    clears.delete()
    return HttpResponseRedirect('/status/')
