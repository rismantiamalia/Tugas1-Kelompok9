from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, update_activity, user_name, clear, update_comment
from .models import Update, Comment
from .forms import Update_Form, Comment_Form

class PageUpdateStatusUnitTest(TestCase):

    def test_page_updatestatus_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_page_update_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_update(self):
        #Creating a new activity
        new_activity = Update.objects.create(activity='Update Profile Dulu')

        #Retrieving all available activity
        counting_all_available_activity = Update.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_form_validation_for_blank_items(self):
        form = Update_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['activity'],
            ["This field is required."]
        )

    def test_post_success_and_render_result(self):
        test = 'Udah Update Belum Ya'
        response = Client().post('/status/update_activity/', {'activity' : test})
        self.assertEqual(response.status_code, 302)

        newresponse = Client().get('/status/')
        html_response = newresponse.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_post_error_and_render_result(self):
        test = 'Udah Update Belum Ya'
        response = Client().post('/status/update_activity/', {'activity' : ''})
        self.assertEqual(response.status_code, 302)

        newresponse = Client().get('/status/')
        html_response = newresponse.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_clear(self):
        new_activity = Update.objects.create(activity='Update Profile Dulu')
        #Retrieving all available activity
        object_id = Update.objects.all().count()
        clear(Update,object_id)

        after_delete = Update.objects.all().count()

        self.assertEqual(after_delete,0)

    def test_comment_success_and_render_result(self):
        new_activity = Update.objects.create(activity='Update Profile Dulu')
        obj_id = str(new_activity.id)
        test = 'Udah Comment Belum Ya'
        response = Client().post('/status/update_comment/' + obj_id, {'comment' : test})
        self.assertEqual(response.status_code, 302)

        newresponse = Client().get('/status/')
        html_response = newresponse.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_comment_error_and_render_result(self):
        new_activity = Update.objects.create(activity='Update Profile Dulu')
        obj_id = str(new_activity.id)

        response = Client().post('/status/update_comment/' + obj_id, {'comment' : ''})
        self.assertEqual(response.status_code, 302)

        newresponse = Client().get('/status/')
        html_response = newresponse.content.decode('utf8')

    def test_Redirect_to_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response, '/status/', 301, 200)