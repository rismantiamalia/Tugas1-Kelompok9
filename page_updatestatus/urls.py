from django.conf.urls import url
from .views import index
from .views import update_activity,clear,update_comment

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'update_activity/$', update_activity, name='update_activity'),
    url(r'^clear/(?P<object_id>[0-9]+)', view=clear, name='clear'),
    url(r'^update_comment/(?P<pk>[0-9]+)', view=update_comment, name='update_comment'),
 ]
