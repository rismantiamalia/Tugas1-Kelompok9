from django import forms

class Update_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    description_attrs = {
        'type': 'text',
        'cols': 60,
        'rows': 10,
        'class': 'update-form-textarea',
        'placeholder':' Whats on your mind. . . '
    }

    activity = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

class Comment_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    description_attrs ={
        'type': 'text',
        'cols': 60,
        'rows': 10,
        'class': 'comment-form-textarea',
        'placeholder':' Write your comment here. . '
    }
    comment = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
