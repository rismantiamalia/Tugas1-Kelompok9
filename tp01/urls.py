"""tp01 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import page_profile.urls as page_profile
import page_updatestatus.urls as page_updatestatus
import page_addfriend.urls as page_addfriend
import page_dashboard.urls as page_dashboard
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile-page/', include(page_profile,namespace='profile-page')),
    url(r'^status/', include(page_updatestatus,namespace='status')),
    url(r'^add-friend/', include(page_addfriend, namespace='add-friend')),
    url(r'^dashboard/', include(page_dashboard, namespace='dashboard')),
    url(r'^$', RedirectView.as_view(url='status/', permanent=True), name='index'),
]
