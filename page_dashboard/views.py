from django.shortcuts import render
from page_updatestatus.models import Update
from page_addfriend.models import Friend
from page_profile.views import user_name
# Create your views here.

response = {}
def index(request):
    response['countFriend'] = Friend.objects.count()
    response['countUpdate'] = Update.objects.count()
    response['lastUpdate'] = Update.objects.last()
    response['name'] = user_name
    return render(request, 'page_dashboard.html', response)

