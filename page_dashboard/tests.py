from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from page_addfriend.models import Friend
from page_updatestatus.models import Update
from page_profile.views import user_name

# Create your tests here.
class DashboardUnitTest(TestCase):
    def test_stats_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_stats_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_stats_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        #Checking whether have Count Friends
        self.assertIn(str(Friend.objects.count()), html_response)

        #Checking whether have Count Feeds
        self.assertIn(str(Update.objects.count()), html_response)

        #Checking whether have username
        self.assertIn(user_name, html_response)

    def test_navbar_and_footer_available(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<nav class="navbar navbar-default navbar-expand-lg" style="background-color: #e6f9ff">',html_response)
        self.assertIn('&copy', html_response)
