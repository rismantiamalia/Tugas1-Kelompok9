from django.db import models
from django.utils import timezone

# Create your models here.
class Friend(models.Model):
    def convert_timezone():
        return timezone.now() + timezone.timedelta(hours=7)
       
    name = models.CharField(max_length=30)
    url = models.URLField(max_length=200)
    created_date = models.DateTimeField(auto_now_add=True)
    time = models.DateTimeField(default=convert_timezone)