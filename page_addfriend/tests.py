from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, new_friend
from .models import Friend
from .forms import Friend_Form

# Create your tests here.

class AddFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)
    
    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)
    
    def test_model_can_create_new_friend(self):
        new_friend = Friend.objects.create(name='Mr. Test',url='http://test.herokuapp.com')

        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend,1)

    def test_form_friend_input_has_placeholder_and_css_classes(self):
        form = Friend_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="id_name"', form.as_p())
        self.assertIn('id="id_url"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'name': '', 'url':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['url'],
            ["This field is required."],
        )

    def test_add_friend_post_success_and_render_the_result(self):
        response = self.client.post('/add-friend/new_friend/', data={'name': 'Haihai', 'url' : 'haihai.com'})
        counting_all_available_activity = Friend.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/add-friend/')

        new_response = self.client.get('/add-friend/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Haihai', html_response)
        self.assertIn('haihai.com', html_response)

    def test_add_friend_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add-friend/new_friend', {'name': '', 'url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)