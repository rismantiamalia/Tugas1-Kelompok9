from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'All inputs are required'
    }

    name_attrs = {
        'type': 'text',
        'class' : 'form-control',
        'placeholder': 'Name'
    }

    url_attrs = {
        'type': 'url',
        'class' : 'form-control',
        'placeholder' : 'http://test.com'
    }

    name = forms.CharField(label='Name ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label='URL ', required=True, max_length=200, widget=forms.URLInput(attrs=url_attrs))