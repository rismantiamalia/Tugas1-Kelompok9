from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

response={}
# Create your views here.
def index(request):
    friend = Friend.objects.all()
    response['friend'] = friend
    html = 'page_addfriend/page_addfriend.html'
    response['friend_form'] = Friend_Form
    return render(request, html, response)

def new_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=response['name'], url=response['url'])
        friend.save()
        return HttpResponseRedirect('/add-friend/')
    else:
        return HttpResponseRedirect('/add-friend/')
