from django.db import models

# Create your models here.

class Profile(models.Model):
	name = models.CharField(max_length=27)
	gender = models.CharField(max_length=6)
	description = models.TextField(max_length=300)
	email = models.EmailField()

class Expertise(models.Model):
	expertise = models.TextField(max_length=100)
