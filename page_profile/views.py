from django.shortcuts import render
from datetime import datetime, date
from .models import Profile, Expertise
# Create your views here.


user_name = 'Kelompok 9 PPW-C' #TODO Implement this, put your name here
birth_date = date(2017,10,13)
birthdate = birth_date.strftime('%d %B') #TODO Implement this, format (Year, Month, Date)
gender = 'Female' #TODO Implement this, put your gender here
descript = 'PPW is fun'
email = 'kelompok9@ppwc.com'


expertise_dict = ['UI/UX','Designer', 'Happy', 'Coding']


def index(request):
	profile = Profile(name=user_name,gender=gender, description=descript, email=email)
	expertise = Expertise(expertise=expertise_dict)
	response = {'Name': profile.name,'Birthday': birthdate,'Gender':profile.gender, 'Description':profile.description, 'Email': profile.email, 'Expert' : expertise.expertise}
	html = 'page_profile.html'
	return render(request, html, response)


