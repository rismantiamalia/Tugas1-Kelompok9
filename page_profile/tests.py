from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Profile, Expertise


# Create your tests here.
class PageProfileUnitTest(TestCase):

	def test_page_profile_url_is_exist(self):
	    response = Client().get('/profile-page/')
	    self.assertEqual(response.status_code, 200)


	def test_page_profile_using_index_func(self):
	    found = resolve('/profile-page/')
	    self.assertEqual(found.func, index)

	#test kalo mau pake model
	def test_model_can_create_profile(self):
		profile = Profile.objects.create(name='Mr. Test',gender='Male', description='Mr. Test description ', email='test@test.com')

		counting_all_available_profile = Profile.objects.all().count()
		self.assertEqual(counting_all_available_profile,1)

