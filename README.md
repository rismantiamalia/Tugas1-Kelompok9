#Tugas Pemrograman 01 Perancangan dan Pemrograman Web

Pipeline status: ![pipeline status](https://gitlab.com/rismantiamalia/Tugas1-Kelompok9/badges/master/pipeline.svg)

Coverage report: ![coverage report](https://gitlab.com/rismantiamalia/Tugas1-Kelompok9/badges/master/coverage.svg)

Link: tugasatuseratus.herokuapp.com

Creator:
1. Kelly William (1606876784)
   Developer Dashbord, NavBar, Footer
2. Nurul Faza Saffanah (1606886186)
   Developer Add Friend
3. Rismanti Amalia N (1606918282)
   Developer Update Status
4. Tyagita Larasati (1606918616)
   Developer Profile

Semoga karya kami dapat dinikmati oleh Anda (:
